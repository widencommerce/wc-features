<?php
/**
 * @file
 * wc_timeline_item.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wc_timeline_item_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wc_timeline_item_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wc_timeline_item_node_info() {
  $items = array(
    'timeline_item' => array(
      'name' => t('Timeline Item'),
      'base' => 'node_content',
      'description' => t('Create a Timeline to display on your site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
