<?php
/**
 * @file
 * wc_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wc_features_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wc_features_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wc_features_node_info() {
  $items = array(
    'feature' => array(
      'name' => t('Feature'),
      'base' => 'node_content',
      'description' => t('Creates a new Feature to display on the landing page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
