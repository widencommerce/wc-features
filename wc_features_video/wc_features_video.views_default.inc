<?php
/**
 * @file
 * wc_features_video.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wc_features_video_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'features_video';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Features Video';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Background Image */
  $handler->display->display_options['fields']['field_features_video_image']['id'] = 'field_features_video_image';
  $handler->display->display_options['fields']['field_features_video_image']['table'] = 'field_data_field_features_video_image';
  $handler->display->display_options['fields']['field_features_video_image']['field'] = 'field_features_video_image';
  $handler->display->display_options['fields']['field_features_video_image']['label'] = '';
  $handler->display->display_options['fields']['field_features_video_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_features_video_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_features_video_image']['settings'] = array(
    'image_style' => 'panopoly_image_original',
    'image_link' => '',
  );
  /* Field: Content: Icon Class */
  $handler->display->display_options['fields']['field_features_video_icon_class']['id'] = 'field_features_video_icon_class';
  $handler->display->display_options['fields']['field_features_video_icon_class']['table'] = 'field_data_field_features_video_icon_class';
  $handler->display->display_options['fields']['field_features_video_icon_class']['field'] = 'field_features_video_icon_class';
  $handler->display->display_options['fields']['field_features_video_icon_class']['label'] = '';
  $handler->display->display_options['fields']['field_features_video_icon_class']['element_label_colon'] = FALSE;
  /* Field: Content: Video */
  $handler->display->display_options['fields']['field_features_video_video']['id'] = 'field_features_video_video';
  $handler->display->display_options['fields']['field_features_video_video']['table'] = 'field_data_field_features_video_video';
  $handler->display->display_options['fields']['field_features_video_video']['field'] = 'field_features_video_video';
  $handler->display->display_options['fields']['field_features_video_video']['label'] = '';
  $handler->display->display_options['fields']['field_features_video_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_features_video_video']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_features_video_video']['type'] = 'file_video';
  $handler->display->display_options['fields']['field_features_video_video']['settings'] = array(
    'controls' => 1,
    'autoplay' => 0,
    'loop' => 0,
    'width' => '',
    'height' => '',
    'multiple_file_behavior' => 'tags',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'features_video' => 'features_video',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['pane_category']['name'] = 'Features Video';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $export['features_video'] = $view;

  return $export;
}
