<?php
/**
 * @file
 * wc_stats.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wc_stats_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wc_stats_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wc_stats_node_info() {
  $items = array(
    'stats' => array(
      'name' => t('Stats'),
      'base' => 'node_content',
      'description' => t('Create a Statistic item to display in the Stats region.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
