<?php
/**
 * @file
 * wc_stats.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function wc_stats_taxonomy_default_vocabularies() {
  return array(
    'statistics_type_category' => array(
      'name' => 'Statistics type category',
      'machine_name' => 'statistics_type_category',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
