<?php
/**
 * @file
 * wc_stats.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wc_stats_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'statistics_bars';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Statistics Bars';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Icon Class */
  $handler->display->display_options['fields']['field_icon_class']['id'] = 'field_icon_class';
  $handler->display->display_options['fields']['field_icon_class']['table'] = 'field_data_field_icon_class';
  $handler->display->display_options['fields']['field_icon_class']['field'] = 'field_icon_class';
  $handler->display->display_options['fields']['field_icon_class']['label'] = '';
  $handler->display->display_options['fields']['field_icon_class']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_icon_class']['type'] = 'text_plain';
  /* Field: Content: Stat */
  $handler->display->display_options['fields']['field_stat']['id'] = 'field_stat';
  $handler->display->display_options['fields']['field_stat']['table'] = 'field_data_field_stat';
  $handler->display->display_options['fields']['field_stat']['field'] = 'field_stat';
  $handler->display->display_options['fields']['field_stat']['label'] = '';
  $handler->display->display_options['fields']['field_stat']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_stat']['type'] = 'text_plain';
  /* Field: Content: Description */
  $handler->display->display_options['fields']['field_stat_description']['id'] = 'field_stat_description';
  $handler->display->display_options['fields']['field_stat_description']['table'] = 'field_data_field_stat_description';
  $handler->display->display_options['fields']['field_stat_description']['field'] = 'field_stat_description';
  $handler->display->display_options['fields']['field_stat_description']['label'] = '';
  $handler->display->display_options['fields']['field_stat_description']['element_label_colon'] = FALSE;
  /* Field: Content: Type */
  $handler->display->display_options['fields']['field_stat_type']['id'] = 'field_stat_type';
  $handler->display->display_options['fields']['field_stat_type']['table'] = 'field_data_field_stat_type';
  $handler->display->display_options['fields']['field_stat_type']['field'] = 'field_stat_type';
  $handler->display->display_options['fields']['field_stat_type']['label'] = '';
  $handler->display->display_options['fields']['field_stat_type']['element_label_colon'] = FALSE;
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'stats' => 'stats',
  );
  /* Filter criterion: Content: Type (field_stat_type) */
  $handler->display->display_options['filters']['field_stat_type_tid']['id'] = 'field_stat_type_tid';
  $handler->display->display_options['filters']['field_stat_type_tid']['table'] = 'field_data_field_stat_type';
  $handler->display->display_options['filters']['field_stat_type_tid']['field'] = 'field_stat_type_tid';
  $handler->display->display_options['filters']['field_stat_type_tid']['value'] = array(
    15 => '15',
  );
  $handler->display->display_options['filters']['field_stat_type_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_stat_type_tid']['vocabulary'] = 'statistics_type_category';

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['pane_category']['name'] = 'Statistics';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $export['statistics_bars'] = $view;

  $view = new view();
  $view->name = 'statistics_charts';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Statistics Charts';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Icon Class */
  $handler->display->display_options['fields']['field_icon_class']['id'] = 'field_icon_class';
  $handler->display->display_options['fields']['field_icon_class']['table'] = 'field_data_field_icon_class';
  $handler->display->display_options['fields']['field_icon_class']['field'] = 'field_icon_class';
  $handler->display->display_options['fields']['field_icon_class']['label'] = '';
  $handler->display->display_options['fields']['field_icon_class']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_icon_class']['type'] = 'text_plain';
  /* Field: Content: Stat */
  $handler->display->display_options['fields']['field_stat']['id'] = 'field_stat';
  $handler->display->display_options['fields']['field_stat']['table'] = 'field_data_field_stat';
  $handler->display->display_options['fields']['field_stat']['field'] = 'field_stat';
  $handler->display->display_options['fields']['field_stat']['label'] = '';
  $handler->display->display_options['fields']['field_stat']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_stat']['type'] = 'text_plain';
  /* Field: Content: Description */
  $handler->display->display_options['fields']['field_stat_description']['id'] = 'field_stat_description';
  $handler->display->display_options['fields']['field_stat_description']['table'] = 'field_data_field_stat_description';
  $handler->display->display_options['fields']['field_stat_description']['field'] = 'field_stat_description';
  $handler->display->display_options['fields']['field_stat_description']['label'] = '';
  $handler->display->display_options['fields']['field_stat_description']['element_label_colon'] = FALSE;
  /* Field: Content: Type */
  $handler->display->display_options['fields']['field_stat_type']['id'] = 'field_stat_type';
  $handler->display->display_options['fields']['field_stat_type']['table'] = 'field_data_field_stat_type';
  $handler->display->display_options['fields']['field_stat_type']['field'] = 'field_stat_type';
  $handler->display->display_options['fields']['field_stat_type']['label'] = '';
  $handler->display->display_options['fields']['field_stat_type']['element_label_colon'] = FALSE;
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'stats' => 'stats',
  );
  /* Filter criterion: Content: Type (field_stat_type) */
  $handler->display->display_options['filters']['field_stat_type_tid']['id'] = 'field_stat_type_tid';
  $handler->display->display_options['filters']['field_stat_type_tid']['table'] = 'field_data_field_stat_type';
  $handler->display->display_options['filters']['field_stat_type_tid']['field'] = 'field_stat_type_tid';
  $handler->display->display_options['filters']['field_stat_type_tid']['value'] = array(
    16 => '16',
  );
  $handler->display->display_options['filters']['field_stat_type_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_stat_type_tid']['vocabulary'] = 'statistics_type_category';

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['pane_category']['name'] = 'Statistics';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $export['statistics_charts'] = $view;

  $view = new view();
  $view->name = 'statistics_counter';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Statistics Counter';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['content'] = '<p class="white-text">We are ready to help you - <a href="#section11">Get Started Now »</a></p>';
  $handler->display->display_options['footer']['area']['format'] = 'panopoly_wysiwyg_text';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Icon Class */
  $handler->display->display_options['fields']['field_icon_class']['id'] = 'field_icon_class';
  $handler->display->display_options['fields']['field_icon_class']['table'] = 'field_data_field_icon_class';
  $handler->display->display_options['fields']['field_icon_class']['field'] = 'field_icon_class';
  $handler->display->display_options['fields']['field_icon_class']['label'] = '';
  $handler->display->display_options['fields']['field_icon_class']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_icon_class']['type'] = 'text_plain';
  /* Field: Content: Stat */
  $handler->display->display_options['fields']['field_stat']['id'] = 'field_stat';
  $handler->display->display_options['fields']['field_stat']['table'] = 'field_data_field_stat';
  $handler->display->display_options['fields']['field_stat']['field'] = 'field_stat';
  $handler->display->display_options['fields']['field_stat']['label'] = '';
  $handler->display->display_options['fields']['field_stat']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_stat']['type'] = 'text_plain';
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'stats' => 'stats',
  );
  /* Filter criterion: Content: Type (field_stat_type) */
  $handler->display->display_options['filters']['field_stat_type_tid']['id'] = 'field_stat_type_tid';
  $handler->display->display_options['filters']['field_stat_type_tid']['table'] = 'field_data_field_stat_type';
  $handler->display->display_options['filters']['field_stat_type_tid']['field'] = 'field_stat_type_tid';
  $handler->display->display_options['filters']['field_stat_type_tid']['value'] = array(
    14 => '14',
  );
  $handler->display->display_options['filters']['field_stat_type_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_stat_type_tid']['vocabulary'] = 'statistics_type_category';

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['pane_category']['name'] = 'Statistics';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $export['statistics_counter'] = $view;

  return $export;
}
