<?php
/**
 * @file
 * wc_portfolio.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wc_portfolio_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'portfolio';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Portfolio';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '6';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Main Image */
  $handler->display->display_options['fields']['field_portfolio_image']['id'] = 'field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['table'] = 'field_data_field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['field'] = 'field_portfolio_image';
  $handler->display->display_options['fields']['field_portfolio_image']['label'] = '';
  $handler->display->display_options['fields']['field_portfolio_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_portfolio_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_portfolio_image']['settings'] = array(
    'image_style' => 'bocor_portfolio',
    'image_link' => '',
  );
  /* Field: Content: Multimedia */
  $handler->display->display_options['fields']['field_portfolio_multimedia']['id'] = 'field_portfolio_multimedia';
  $handler->display->display_options['fields']['field_portfolio_multimedia']['table'] = 'field_data_field_portfolio_multimedia';
  $handler->display->display_options['fields']['field_portfolio_multimedia']['field'] = 'field_portfolio_multimedia';
  $handler->display->display_options['fields']['field_portfolio_multimedia']['label'] = '';
  $handler->display->display_options['fields']['field_portfolio_multimedia']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_portfolio_multimedia']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_portfolio_multimedia']['delta_limit'] = '0';
  $handler->display->display_options['fields']['field_portfolio_multimedia']['delta_offset'] = '0';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Author Name */
  $handler->display->display_options['fields']['field_portfolio_author_name']['id'] = 'field_portfolio_author_name';
  $handler->display->display_options['fields']['field_portfolio_author_name']['table'] = 'field_data_field_portfolio_author_name';
  $handler->display->display_options['fields']['field_portfolio_author_name']['field'] = 'field_portfolio_author_name';
  $handler->display->display_options['fields']['field_portfolio_author_name']['label'] = '';
  $handler->display->display_options['fields']['field_portfolio_author_name']['element_label_colon'] = FALSE;
  /* Field: Content: Category */
  $handler->display->display_options['fields']['field_portfolio_category']['id'] = 'field_portfolio_category';
  $handler->display->display_options['fields']['field_portfolio_category']['table'] = 'field_data_field_portfolio_category';
  $handler->display->display_options['fields']['field_portfolio_category']['field'] = 'field_portfolio_category';
  $handler->display->display_options['fields']['field_portfolio_category']['label'] = '';
  $handler->display->display_options['fields']['field_portfolio_category']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_portfolio_category']['delta_offset'] = '0';
  /* Field: Content: Company */
  $handler->display->display_options['fields']['field_portfolio_company']['id'] = 'field_portfolio_company';
  $handler->display->display_options['fields']['field_portfolio_company']['table'] = 'field_data_field_portfolio_company';
  $handler->display->display_options['fields']['field_portfolio_company']['field'] = 'field_portfolio_company';
  $handler->display->display_options['fields']['field_portfolio_company']['label'] = '';
  $handler->display->display_options['fields']['field_portfolio_company']['element_label_colon'] = FALSE;
  /* Field: Content: Link url */
  $handler->display->display_options['fields']['field_portfolio_link']['id'] = 'field_portfolio_link';
  $handler->display->display_options['fields']['field_portfolio_link']['table'] = 'field_data_field_portfolio_link';
  $handler->display->display_options['fields']['field_portfolio_link']['field'] = 'field_portfolio_link';
  $handler->display->display_options['fields']['field_portfolio_link']['label'] = '';
  $handler->display->display_options['fields']['field_portfolio_link']['element_label_colon'] = FALSE;
  /* Field: Content: Large Image */
  $handler->display->display_options['fields']['field_portfolio_large_image']['id'] = 'field_portfolio_large_image';
  $handler->display->display_options['fields']['field_portfolio_large_image']['table'] = 'field_data_field_portfolio_large_image';
  $handler->display->display_options['fields']['field_portfolio_large_image']['field'] = 'field_portfolio_large_image';
  $handler->display->display_options['fields']['field_portfolio_large_image']['label'] = '';
  $handler->display->display_options['fields']['field_portfolio_large_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_portfolio_large_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_portfolio_large_image']['settings'] = array(
    'image_style' => 'panopoly_image_original',
    'image_link' => '',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'portfolio' => 'portfolio',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['pane_category']['name'] = 'Portfolio';
  $handler->display->display_options['pane_category']['weight'] = '0';

  /* Display: Portfolio page */
  $handler = $view->new_display('panel_pane', 'Portfolio page', 'panel_pane_2');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '9';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pane_category']['name'] = 'Portfolio';
  $handler->display->display_options['pane_category']['weight'] = '0';

  /* Display: Recent Work */
  $handler = $view->new_display('panel_pane', 'Recent Work', 'panel_pane_3');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pane_category']['name'] = 'Portfolio';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $translatables['portfolio'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Content pane'),
    t('Portfolio'),
    t('Portfolio page'),
    t('Recent Work'),
  );
  $export['portfolio'] = $view;

  return $export;
}
