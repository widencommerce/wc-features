<?php
/**
 * @file
 * wc_portfolio.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function wc_portfolio_taxonomy_default_vocabularies() {
  return array(
    'portfolio_category' => array(
      'name' => 'Portfolio Category',
      'machine_name' => 'portfolio_category',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
    ),
  );
}
