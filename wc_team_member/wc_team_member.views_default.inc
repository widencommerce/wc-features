<?php
/**
 * @file
 * wc_team_member.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wc_team_member_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'team';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Team';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Job Title */
  $handler->display->display_options['fields']['field_job_title']['id'] = 'field_job_title';
  $handler->display->display_options['fields']['field_job_title']['table'] = 'field_data_field_job_title';
  $handler->display->display_options['fields']['field_job_title']['field'] = 'field_job_title';
  $handler->display->display_options['fields']['field_job_title']['label'] = '';
  $handler->display->display_options['fields']['field_job_title']['element_label_colon'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Profile */
  $handler->display->display_options['fields']['field_profile']['id'] = 'field_profile';
  $handler->display->display_options['fields']['field_profile']['table'] = 'field_data_field_profile';
  $handler->display->display_options['fields']['field_profile']['field'] = 'field_profile';
  $handler->display->display_options['fields']['field_profile']['label'] = '';
  $handler->display->display_options['fields']['field_profile']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_profile']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_profile']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Facebook URL */
  $handler->display->display_options['fields']['field_facebook_url']['id'] = 'field_facebook_url';
  $handler->display->display_options['fields']['field_facebook_url']['table'] = 'field_data_field_facebook_url';
  $handler->display->display_options['fields']['field_facebook_url']['field'] = 'field_facebook_url';
  $handler->display->display_options['fields']['field_facebook_url']['label'] = '';
  $handler->display->display_options['fields']['field_facebook_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_facebook_url']['type'] = 'text_plain';
  /* Field: Content: Facebook Tooltip */
  $handler->display->display_options['fields']['field_tm_facebook_tooltip']['id'] = 'field_tm_facebook_tooltip';
  $handler->display->display_options['fields']['field_tm_facebook_tooltip']['table'] = 'field_data_field_tm_facebook_tooltip';
  $handler->display->display_options['fields']['field_tm_facebook_tooltip']['field'] = 'field_tm_facebook_tooltip';
  $handler->display->display_options['fields']['field_tm_facebook_tooltip']['label'] = '';
  $handler->display->display_options['fields']['field_tm_facebook_tooltip']['element_label_colon'] = FALSE;
  /* Field: Content: Pinterest URL */
  $handler->display->display_options['fields']['field_pinterest_url']['id'] = 'field_pinterest_url';
  $handler->display->display_options['fields']['field_pinterest_url']['table'] = 'field_data_field_pinterest_url';
  $handler->display->display_options['fields']['field_pinterest_url']['field'] = 'field_pinterest_url';
  $handler->display->display_options['fields']['field_pinterest_url']['label'] = '';
  $handler->display->display_options['fields']['field_pinterest_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_pinterest_url']['type'] = 'text_plain';
  /* Field: Content: Pinterest Tooltip */
  $handler->display->display_options['fields']['field_tm_pinterest_tooltip']['id'] = 'field_tm_pinterest_tooltip';
  $handler->display->display_options['fields']['field_tm_pinterest_tooltip']['table'] = 'field_data_field_tm_pinterest_tooltip';
  $handler->display->display_options['fields']['field_tm_pinterest_tooltip']['field'] = 'field_tm_pinterest_tooltip';
  $handler->display->display_options['fields']['field_tm_pinterest_tooltip']['label'] = '';
  $handler->display->display_options['fields']['field_tm_pinterest_tooltip']['element_label_colon'] = FALSE;
  /* Field: Content: Twitter URL */
  $handler->display->display_options['fields']['field_twitter_url']['id'] = 'field_twitter_url';
  $handler->display->display_options['fields']['field_twitter_url']['table'] = 'field_data_field_twitter_url';
  $handler->display->display_options['fields']['field_twitter_url']['field'] = 'field_twitter_url';
  $handler->display->display_options['fields']['field_twitter_url']['label'] = '';
  $handler->display->display_options['fields']['field_twitter_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_twitter_url']['type'] = 'text_plain';
  /* Field: Content: Twitter Tooltip */
  $handler->display->display_options['fields']['field_tm_twitter_tooltip']['id'] = 'field_tm_twitter_tooltip';
  $handler->display->display_options['fields']['field_tm_twitter_tooltip']['table'] = 'field_data_field_tm_twitter_tooltip';
  $handler->display->display_options['fields']['field_tm_twitter_tooltip']['field'] = 'field_tm_twitter_tooltip';
  $handler->display->display_options['fields']['field_tm_twitter_tooltip']['label'] = '';
  $handler->display->display_options['fields']['field_tm_twitter_tooltip']['element_label_colon'] = FALSE;
  /* Field: Content: Linkedin URL */
  $handler->display->display_options['fields']['field_linkedin_url']['id'] = 'field_linkedin_url';
  $handler->display->display_options['fields']['field_linkedin_url']['table'] = 'field_data_field_linkedin_url';
  $handler->display->display_options['fields']['field_linkedin_url']['field'] = 'field_linkedin_url';
  $handler->display->display_options['fields']['field_linkedin_url']['label'] = '';
  $handler->display->display_options['fields']['field_linkedin_url']['element_label_colon'] = FALSE;
  /* Field: Content: Linkedin Tooltip */
  $handler->display->display_options['fields']['field_tm_linkedin_tooltip']['id'] = 'field_tm_linkedin_tooltip';
  $handler->display->display_options['fields']['field_tm_linkedin_tooltip']['table'] = 'field_data_field_tm_linkedin_tooltip';
  $handler->display->display_options['fields']['field_tm_linkedin_tooltip']['field'] = 'field_tm_linkedin_tooltip';
  $handler->display->display_options['fields']['field_tm_linkedin_tooltip']['label'] = '';
  $handler->display->display_options['fields']['field_tm_linkedin_tooltip']['element_label_colon'] = FALSE;
  /* Field: Content: Google Plus URL */
  $handler->display->display_options['fields']['field_google_plus_url']['id'] = 'field_google_plus_url';
  $handler->display->display_options['fields']['field_google_plus_url']['table'] = 'field_data_field_google_plus_url';
  $handler->display->display_options['fields']['field_google_plus_url']['field'] = 'field_google_plus_url';
  $handler->display->display_options['fields']['field_google_plus_url']['label'] = '';
  $handler->display->display_options['fields']['field_google_plus_url']['element_label_colon'] = FALSE;
  /* Field: Content: Google Plus Tooltip */
  $handler->display->display_options['fields']['field_tm_google_plus_tooltip']['id'] = 'field_tm_google_plus_tooltip';
  $handler->display->display_options['fields']['field_tm_google_plus_tooltip']['table'] = 'field_data_field_tm_google_plus_tooltip';
  $handler->display->display_options['fields']['field_tm_google_plus_tooltip']['field'] = 'field_tm_google_plus_tooltip';
  $handler->display->display_options['fields']['field_tm_google_plus_tooltip']['label'] = '';
  $handler->display->display_options['fields']['field_tm_google_plus_tooltip']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'team_member' => 'team_member',
  );
  /* Filter criterion: Content: Are you the owner (field_tm_are_you_the_owner) */
  $handler->display->display_options['filters']['field_tm_are_you_the_owner_value']['id'] = 'field_tm_are_you_the_owner_value';
  $handler->display->display_options['filters']['field_tm_are_you_the_owner_value']['table'] = 'field_data_field_tm_are_you_the_owner';
  $handler->display->display_options['filters']['field_tm_are_you_the_owner_value']['field'] = 'field_tm_are_you_the_owner_value';
  $handler->display->display_options['filters']['field_tm_are_you_the_owner_value']['value'] = array(
    1 => '1',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'team_member' => 'team_member',
  );
  $handler->display->display_options['pane_category']['name'] = 'Team';
  $handler->display->display_options['pane_category']['weight'] = '0';

  /* Display: Owner */
  $handler = $view->new_display('panel_pane', 'Owner', 'panel_pane_2');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pane_category']['name'] = 'Team';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $translatables['team'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Content pane'),
    t('Team'),
    t('Owner'),
  );
  $export['team'] = $view;

  return $export;
}
