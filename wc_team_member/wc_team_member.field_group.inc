<?php
/**
 * @file
 * wc_team_member.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function wc_team_member_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_social_network|node|team_member|form';
  $field_group->group_name = 'group_social_network';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'team_member';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Social Network',
    'weight' => '8',
    'children' => array(
      0 => 'field_facebook_url',
      1 => 'field_google_plus_url',
      2 => 'field_linkedin_url',
      3 => 'field_pinterest_url',
      4 => 'field_twitter_url',
      5 => 'field_tm_twitter_tooltip',
      6 => 'field_tm_facebook_tooltip',
      7 => 'field_tm_pinterest_tooltip',
      8 => 'field_tm_google_plus_tooltip',
      9 => 'field_tm_linkedin_tooltip',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-social-network field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_social_network|node|team_member|form'] = $field_group;

  return $export;
}
