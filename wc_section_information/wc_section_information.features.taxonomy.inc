<?php
/**
 * @file
 * wc_section_information.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function wc_section_information_taxonomy_default_vocabularies() {
  return array(
    'section_information_category' => array(
      'name' => 'Section Information Category',
      'machine_name' => 'section_information_category',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
    ),
  );
}
