<?php
/**
 * @file
 * wc_section_information.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wc_section_information_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function wc_section_information_node_info() {
  $items = array(
    'section_information' => array(
      'name' => t('Section Information'),
      'base' => 'node_content',
      'description' => t('Create additional information for your site sections.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
