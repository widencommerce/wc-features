<?php
/**
 * @file
 * wc_location.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wc_location_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'location';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Location';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Address */
  $handler->display->display_options['fields']['field_location_address']['id'] = 'field_location_address';
  $handler->display->display_options['fields']['field_location_address']['table'] = 'field_data_field_location_address';
  $handler->display->display_options['fields']['field_location_address']['field'] = 'field_location_address';
  $handler->display->display_options['fields']['field_location_address']['label'] = '';
  $handler->display->display_options['fields']['field_location_address']['element_label_colon'] = FALSE;
  /* Field: Content: Email */
  $handler->display->display_options['fields']['field_location_email']['id'] = 'field_location_email';
  $handler->display->display_options['fields']['field_location_email']['table'] = 'field_data_field_location_email';
  $handler->display->display_options['fields']['field_location_email']['field'] = 'field_location_email';
  $handler->display->display_options['fields']['field_location_email']['label'] = '';
  $handler->display->display_options['fields']['field_location_email']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_location_image']['id'] = 'field_location_image';
  $handler->display->display_options['fields']['field_location_image']['table'] = 'field_data_field_location_image';
  $handler->display->display_options['fields']['field_location_image']['field'] = 'field_location_image';
  $handler->display->display_options['fields']['field_location_image']['label'] = '';
  $handler->display->display_options['fields']['field_location_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_location_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_location_image']['settings'] = array(
    'image_style' => 'panopoly_image_thumbnail',
    'image_link' => '',
  );
  /* Field: Content: Location */
  $handler->display->display_options['fields']['field_jobs_location']['id'] = 'field_jobs_location';
  $handler->display->display_options['fields']['field_jobs_location']['table'] = 'field_data_field_jobs_location';
  $handler->display->display_options['fields']['field_jobs_location']['field'] = 'field_jobs_location';
  $handler->display->display_options['fields']['field_jobs_location']['label'] = '';
  $handler->display->display_options['fields']['field_jobs_location']['element_label_colon'] = FALSE;
  /* Field: Content: Phone */
  $handler->display->display_options['fields']['field_location_phone']['id'] = 'field_location_phone';
  $handler->display->display_options['fields']['field_location_phone']['table'] = 'field_data_field_location_phone';
  $handler->display->display_options['fields']['field_location_phone']['field'] = 'field_location_phone';
  $handler->display->display_options['fields']['field_location_phone']['label'] = '';
  $handler->display->display_options['fields']['field_location_phone']['element_label_colon'] = FALSE;
  /* Field: Content: Website */
  $handler->display->display_options['fields']['field_location_website']['id'] = 'field_location_website';
  $handler->display->display_options['fields']['field_location_website']['table'] = 'field_data_field_location_website';
  $handler->display->display_options['fields']['field_location_website']['field'] = 'field_location_website';
  $handler->display->display_options['fields']['field_location_website']['label'] = '';
  $handler->display->display_options['fields']['field_location_website']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'location' => 'location',
  );
  $export['location'] = $view;

  return $export;
}
