<?php
/**
 * @file
 * wc_company_contact_information.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wc_company_contact_information_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function wc_company_contact_information_node_info() {
  $items = array(
    'company_contact_information' => array(
      'name' => t('Company Contact Information'),
      'base' => 'node_content',
      'description' => t('Entering you company contact information.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
