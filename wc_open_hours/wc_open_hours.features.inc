<?php
/**
 * @file
 * wc_open_hours.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wc_open_hours_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function wc_open_hours_node_info() {
  $items = array(
    'open_hours' => array(
      'name' => t('Open hours'),
      'base' => 'node_content',
      'description' => t('Create Open Hours to display on the site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
