<?php
/**
 * @file
 * wc_pricing.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wc_pricing_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wc_pricing_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wc_pricing_node_info() {
  $items = array(
    'pricing' => array(
      'name' => t('Pricing'),
      'base' => 'node_content',
      'description' => t('Create Pricing to display on the site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
