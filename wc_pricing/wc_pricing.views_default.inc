<?php
/**
 * @file
 * wc_pricing.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wc_pricing_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'pricing';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Pricing';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Button Text */
  $handler->display->display_options['fields']['field_pricing_button_text']['id'] = 'field_pricing_button_text';
  $handler->display->display_options['fields']['field_pricing_button_text']['table'] = 'field_data_field_pricing_button_text';
  $handler->display->display_options['fields']['field_pricing_button_text']['field'] = 'field_pricing_button_text';
  $handler->display->display_options['fields']['field_pricing_button_text']['label'] = '';
  $handler->display->display_options['fields']['field_pricing_button_text']['element_label_colon'] = FALSE;
  /* Field: Content: Currency */
  $handler->display->display_options['fields']['field_pricing_currency']['id'] = 'field_pricing_currency';
  $handler->display->display_options['fields']['field_pricing_currency']['table'] = 'field_data_field_pricing_currency';
  $handler->display->display_options['fields']['field_pricing_currency']['field'] = 'field_pricing_currency';
  $handler->display->display_options['fields']['field_pricing_currency']['label'] = '';
  $handler->display->display_options['fields']['field_pricing_currency']['element_label_colon'] = FALSE;
  /* Field: Content: Description */
  $handler->display->display_options['fields']['field_pricing_description']['id'] = 'field_pricing_description';
  $handler->display->display_options['fields']['field_pricing_description']['table'] = 'field_data_field_pricing_description';
  $handler->display->display_options['fields']['field_pricing_description']['field'] = 'field_pricing_description';
  $handler->display->display_options['fields']['field_pricing_description']['label'] = '';
  $handler->display->display_options['fields']['field_pricing_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_pricing_description']['delta_offset'] = '0';
  /* Field: Content: Icon Class */
  $handler->display->display_options['fields']['field_pricing_icon_class']['id'] = 'field_pricing_icon_class';
  $handler->display->display_options['fields']['field_pricing_icon_class']['table'] = 'field_data_field_pricing_icon_class';
  $handler->display->display_options['fields']['field_pricing_icon_class']['field'] = 'field_pricing_icon_class';
  $handler->display->display_options['fields']['field_pricing_icon_class']['label'] = '';
  $handler->display->display_options['fields']['field_pricing_icon_class']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_pricing_image']['id'] = 'field_pricing_image';
  $handler->display->display_options['fields']['field_pricing_image']['table'] = 'field_data_field_pricing_image';
  $handler->display->display_options['fields']['field_pricing_image']['field'] = 'field_pricing_image';
  $handler->display->display_options['fields']['field_pricing_image']['label'] = '';
  $handler->display->display_options['fields']['field_pricing_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_pricing_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_pricing_image']['settings'] = array(
    'image_style' => 'panopoly_image_thumbnail',
    'image_link' => '',
  );
  /* Field: Content: Link url */
  $handler->display->display_options['fields']['field_pricing_link_url']['id'] = 'field_pricing_link_url';
  $handler->display->display_options['fields']['field_pricing_link_url']['table'] = 'field_data_field_pricing_link_url';
  $handler->display->display_options['fields']['field_pricing_link_url']['field'] = 'field_pricing_link_url';
  $handler->display->display_options['fields']['field_pricing_link_url']['label'] = '';
  $handler->display->display_options['fields']['field_pricing_link_url']['element_label_colon'] = FALSE;
  /* Field: Content: Price */
  $handler->display->display_options['fields']['field_pricing_price']['id'] = 'field_pricing_price';
  $handler->display->display_options['fields']['field_pricing_price']['table'] = 'field_data_field_pricing_price';
  $handler->display->display_options['fields']['field_pricing_price']['field'] = 'field_pricing_price';
  $handler->display->display_options['fields']['field_pricing_price']['label'] = '';
  $handler->display->display_options['fields']['field_pricing_price']['element_label_colon'] = FALSE;
  /* Field: Content: Time */
  $handler->display->display_options['fields']['field_pricing_time']['id'] = 'field_pricing_time';
  $handler->display->display_options['fields']['field_pricing_time']['table'] = 'field_data_field_pricing_time';
  $handler->display->display_options['fields']['field_pricing_time']['field'] = 'field_pricing_time';
  $handler->display->display_options['fields']['field_pricing_time']['label'] = '';
  $handler->display->display_options['fields']['field_pricing_time']['element_label_colon'] = FALSE;
  /* Field: Content: Featured */
  $handler->display->display_options['fields']['field_pricing_featured']['id'] = 'field_pricing_featured';
  $handler->display->display_options['fields']['field_pricing_featured']['table'] = 'field_data_field_pricing_featured';
  $handler->display->display_options['fields']['field_pricing_featured']['field'] = 'field_pricing_featured';
  $handler->display->display_options['fields']['field_pricing_featured']['label'] = '';
  $handler->display->display_options['fields']['field_pricing_featured']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'pricing' => 'pricing',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['pane_category']['name'] = 'Pricing';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $translatables['pricing'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Content pane'),
    t('Pricing'),
  );
  $export['pricing'] = $view;

  return $export;
}
