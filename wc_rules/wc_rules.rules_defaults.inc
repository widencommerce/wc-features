<?php
/**
 * @file
 * wc_rules.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function wc_rules_default_rules_configuration() {
  $items = array();
  $items['rules_return_to_content_list_after_adding_a_new_content'] = entity_import('rules_config', '{ "rules_return_to_content_list_after_adding_a_new_content" : {
      "LABEL" : "Return to content list after adding a new content",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert" : [] },
      "DO" : [ { "redirect" : { "url" : "\\/admin\\/content" } } ]
    }
  }');
  return $items;
}
