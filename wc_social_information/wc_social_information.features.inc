<?php
/**
 * @file
 * wc_social_information.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wc_social_information_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function wc_social_information_node_info() {
  $items = array(
    'social_informaton' => array(
      'name' => t('Social Informaton'),
      'base' => 'node_content',
      'description' => t('Proving you social information for you site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
