<?php
/**
 * @file
 * wc_jobs.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wc_jobs_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'jobs';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Jobs';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Category */
  $handler->display->display_options['fields']['field_jobs_category']['id'] = 'field_jobs_category';
  $handler->display->display_options['fields']['field_jobs_category']['table'] = 'field_data_field_jobs_category';
  $handler->display->display_options['fields']['field_jobs_category']['field'] = 'field_jobs_category';
  $handler->display->display_options['fields']['field_jobs_category']['label'] = '';
  $handler->display->display_options['fields']['field_jobs_category']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_jobs_category']['delta_offset'] = '0';
  /* Field: Content: Icon Class */
  $handler->display->display_options['fields']['field_icon_class']['id'] = 'field_icon_class';
  $handler->display->display_options['fields']['field_icon_class']['table'] = 'field_data_field_icon_class';
  $handler->display->display_options['fields']['field_icon_class']['field'] = 'field_icon_class';
  $handler->display->display_options['fields']['field_icon_class']['label'] = '';
  $handler->display->display_options['fields']['field_icon_class']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_jobs_image']['id'] = 'field_jobs_image';
  $handler->display->display_options['fields']['field_jobs_image']['table'] = 'field_data_field_jobs_image';
  $handler->display->display_options['fields']['field_jobs_image']['field'] = 'field_jobs_image';
  $handler->display->display_options['fields']['field_jobs_image']['label'] = '';
  $handler->display->display_options['fields']['field_jobs_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_jobs_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_jobs_image']['settings'] = array(
    'image_style' => 'panopoly_image_thumbnail',
    'image_link' => '',
  );
  /* Field: Content: Location */
  $handler->display->display_options['fields']['field_jobs_location']['id'] = 'field_jobs_location';
  $handler->display->display_options['fields']['field_jobs_location']['table'] = 'field_data_field_jobs_location';
  $handler->display->display_options['fields']['field_jobs_location']['field'] = 'field_jobs_location';
  $handler->display->display_options['fields']['field_jobs_location']['label'] = '';
  $handler->display->display_options['fields']['field_jobs_location']['element_label_colon'] = FALSE;
  /* Field: Content: Possition */
  $handler->display->display_options['fields']['field_jobs_possition']['id'] = 'field_jobs_possition';
  $handler->display->display_options['fields']['field_jobs_possition']['table'] = 'field_data_field_jobs_possition';
  $handler->display->display_options['fields']['field_jobs_possition']['field'] = 'field_jobs_possition';
  $handler->display->display_options['fields']['field_jobs_possition']['label'] = '';
  $handler->display->display_options['fields']['field_jobs_possition']['element_label_colon'] = FALSE;
  /* Field: Content: Requirement */
  $handler->display->display_options['fields']['field_jobs_requirement']['id'] = 'field_jobs_requirement';
  $handler->display->display_options['fields']['field_jobs_requirement']['table'] = 'field_data_field_jobs_requirement';
  $handler->display->display_options['fields']['field_jobs_requirement']['field'] = 'field_jobs_requirement';
  $handler->display->display_options['fields']['field_jobs_requirement']['label'] = '';
  $handler->display->display_options['fields']['field_jobs_requirement']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_jobs_requirement']['delta_offset'] = '0';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'jobs' => 'jobs',
  );
  $export['jobs'] = $view;

  return $export;
}
