<?php
/**
 * @file
 * wc_jobs.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wc_jobs_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wc_jobs_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wc_jobs_node_info() {
  $items = array(
    'jobs' => array(
      'name' => t('Jobs'),
      'base' => 'node_content',
      'description' => t('Create Jobs to display on the site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
