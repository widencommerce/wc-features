<?php
/**
 * @file
 * wc_jobs.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function wc_jobs_taxonomy_default_vocabularies() {
  return array(
    'jobs_category' => array(
      'name' => 'Jobs Category',
      'machine_name' => 'jobs_category',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
