<?php
/**
 * @file
 * wc_blog.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wc_blog_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'blog';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Blog';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '6';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_blogs_image']['id'] = 'field_blogs_image';
  $handler->display->display_options['fields']['field_blogs_image']['table'] = 'field_data_field_blogs_image';
  $handler->display->display_options['fields']['field_blogs_image']['field'] = 'field_blogs_image';
  $handler->display->display_options['fields']['field_blogs_image']['label'] = '';
  $handler->display->display_options['fields']['field_blogs_image']['element_class'] = 'img-responsive';
  $handler->display->display_options['fields']['field_blogs_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_blogs_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_blogs_image']['settings'] = array(
    'image_style' => 'panopoly_image_original',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = '';
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created']['custom_date_format'] = 'd-M-Y';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'panopoly_time';
  /* Field: Content: Categories */
  $handler->display->display_options['fields']['field_blogs_categories']['id'] = 'field_blogs_categories';
  $handler->display->display_options['fields']['field_blogs_categories']['table'] = 'field_data_field_blogs_categories';
  $handler->display->display_options['fields']['field_blogs_categories']['field'] = 'field_blogs_categories';
  $handler->display->display_options['fields']['field_blogs_categories']['label'] = '';
  $handler->display->display_options['fields']['field_blogs_categories']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_blogs_categories']['type'] = 'taxonomy_term_reference_link';
  $handler->display->display_options['fields']['field_blogs_categories']['delta_offset'] = '0';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '800',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Categories (field_blogs_categories) */
  $handler->display->display_options['arguments']['field_blogs_categories_tid']['id'] = 'field_blogs_categories_tid';
  $handler->display->display_options['arguments']['field_blogs_categories_tid']['table'] = 'field_data_field_blogs_categories';
  $handler->display->display_options['arguments']['field_blogs_categories_tid']['field'] = 'field_blogs_categories_tid';
  $handler->display->display_options['arguments']['field_blogs_categories_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_blogs_categories_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_blogs_categories_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_blogs_categories_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_blogs_categories_tid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['field_blogs_categories_tid']['validate']['type'] = 'taxonomy_term';
  $handler->display->display_options['arguments']['field_blogs_categories_tid']['validate_options']['vocabularies'] = array(
    'blog_category' => 'blog_category',
  );
  $handler->display->display_options['arguments']['field_blogs_categories_tid']['validate_options']['type'] = 'name';
  $handler->display->display_options['arguments']['field_blogs_categories_tid']['validate']['fail'] = 'ignore';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'blog' => 'blog',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['enabled'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['path'] = 'blog';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Blog';
  $handler->display->display_options['menu']['name'] = 'main-menu';

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['pane_category']['name'] = 'Blog';
  $handler->display->display_options['pane_category']['weight'] = '0';

  /* Display: Blog Page */
  $handler = $view->new_display('panel_pane', 'Blog Page', 'panel_pane_2');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pane_category']['name'] = 'Blog';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $translatables['blog'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('All'),
    t('Page'),
    t('Content pane'),
    t('Blog'),
    t('Blog Page'),
  );
  $export['blog'] = $view;

  return $export;
}
