<?php
/**
 * @file
 * wc_roles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function wc_roles_user_default_roles() {
  $roles = array();

  // Exported role: site administrator.
  $roles['site administrator'] = array(
    'name' => 'site administrator',
    'weight' => 4,
  );

  return $roles;
}
