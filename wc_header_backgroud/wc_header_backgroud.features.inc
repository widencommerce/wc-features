<?php
/**
 * @file
 * wc_header_backgroud.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wc_header_backgroud_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function wc_header_backgroud_node_info() {
  $items = array(
    'header_background' => array(
      'name' => t('Header Backgroud'),
      'base' => 'node_content',
      'description' => t('Adding a backgroud image to your site header.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
