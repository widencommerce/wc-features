<?php
/**
 * @file
 * stamp_screenshots.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function stamp_screenshots_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function stamp_screenshots_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function stamp_screenshots_node_info() {
  $items = array(
    'screenshots' => array(
      'name' => t('Screenshots'),
      'base' => 'node_content',
      'description' => t('Create screen shots to display on the landing page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
