<?php
/**
 * @file
 * wc_callout.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wc_callout_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function wc_callout_node_info() {
  $items = array(
    'call_out' => array(
      'name' => t('Call Out'),
      'base' => 'node_content',
      'description' => t('Create a call out to display on the site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
