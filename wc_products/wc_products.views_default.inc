<?php
/**
 * @file
 * wc_products.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wc_products_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'products';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Products';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<!-- SECTION HEADER -->
            <div class="section-header">
                <div class="small-text-medium uppercase colored-text">
                    Pricing
                </div>
                <h2 class="dark-text"><strong>Affordable</strong> Packages</h2>
                <div class="colored-line">
                </div>
                <div class="sub-heading">
                    Cloud computing subscription model out of the box proactive solution.
                </div>
            </div>';
  $handler->display->display_options['header']['area']['format'] = 'panopoly_wysiwyg_text';
  /* Relationship: Content: Referenced products */
  $handler->display->display_options['relationships']['field_product_product_id']['id'] = 'field_product_product_id';
  $handler->display->display_options['relationships']['field_product_product_id']['table'] = 'field_data_field_product';
  $handler->display->display_options['relationships']['field_product_product_id']['field'] = 'field_product_product_id';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Blurb */
  $handler->display->display_options['fields']['field_blurb']['id'] = 'field_blurb';
  $handler->display->display_options['fields']['field_blurb']['table'] = 'field_data_field_blurb';
  $handler->display->display_options['fields']['field_blurb']['field'] = 'field_blurb';
  $handler->display->display_options['fields']['field_blurb']['label'] = '';
  $handler->display->display_options['fields']['field_blurb']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_blurb']['type'] = 'text_plain';
  /* Field: Content: File Storage */
  $handler->display->display_options['fields']['field_file_storage']['id'] = 'field_file_storage';
  $handler->display->display_options['fields']['field_file_storage']['table'] = 'field_data_field_file_storage';
  $handler->display->display_options['fields']['field_file_storage']['field'] = 'field_file_storage';
  $handler->display->display_options['fields']['field_file_storage']['label'] = '';
  $handler->display->display_options['fields']['field_file_storage']['element_label_colon'] = FALSE;
  /* Field: Content: Projects Per Month */
  $handler->display->display_options['fields']['field_projects_per_month']['id'] = 'field_projects_per_month';
  $handler->display->display_options['fields']['field_projects_per_month']['table'] = 'field_data_field_projects_per_month';
  $handler->display->display_options['fields']['field_projects_per_month']['field'] = 'field_projects_per_month';
  $handler->display->display_options['fields']['field_projects_per_month']['label'] = '';
  $handler->display->display_options['fields']['field_projects_per_month']['element_label_colon'] = FALSE;
  /* Field: Content: Promoted Product */
  $handler->display->display_options['fields']['field_promoted_product']['id'] = 'field_promoted_product';
  $handler->display->display_options['fields']['field_promoted_product']['table'] = 'field_data_field_promoted_product';
  $handler->display->display_options['fields']['field_promoted_product']['field'] = 'field_promoted_product';
  $handler->display->display_options['fields']['field_promoted_product']['label'] = '';
  $handler->display->display_options['fields']['field_promoted_product']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_promoted_product']['type'] = 'list_key';
  /* Field: Content: User Access */
  $handler->display->display_options['fields']['field_user_access']['id'] = 'field_user_access';
  $handler->display->display_options['fields']['field_user_access']['table'] = 'field_data_field_user_access';
  $handler->display->display_options['fields']['field_user_access']['field'] = 'field_user_access';
  $handler->display->display_options['fields']['field_user_access']['label'] = '';
  $handler->display->display_options['fields']['field_user_access']['element_label_colon'] = FALSE;
  /* Field: Commerce Product: Price */
  $handler->display->display_options['fields']['commerce_price']['id'] = 'commerce_price';
  $handler->display->display_options['fields']['commerce_price']['table'] = 'field_data_commerce_price';
  $handler->display->display_options['fields']['commerce_price']['field'] = 'commerce_price';
  $handler->display->display_options['fields']['commerce_price']['relationship'] = 'field_product_product_id';
  $handler->display->display_options['fields']['commerce_price']['label'] = '';
  $handler->display->display_options['fields']['commerce_price']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['commerce_price']['click_sort_column'] = 'amount';
  $handler->display->display_options['fields']['commerce_price']['settings'] = array(
    'calculation' => '0',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'product' => 'product',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $export['products'] = $view;

  return $export;
}
