<?php
/**
 * @file
 * wc_google_maps.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wc_google_maps_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function wc_google_maps_node_info() {
  $items = array(
    'google_maps' => array(
      'name' => t('Google Maps'),
      'base' => 'node_content',
      'description' => t('Provide the latitude and longitude of you business location.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
